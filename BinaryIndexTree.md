```
class BinaryIndexedTree {
    private int[] BITree;
    private int size;

    public BinaryIndexedTree(int size) {
        this.size = size;
        this.BITree = new int[size + 1];
    }

    // Update element at index i by adding val
    public void update(int i, int val) {
        i++; // BITree is 1-indexed
        while (i <= size) {
            BITree[i] += val;
            i += i & -i;
        }
    }

    // Get prefix sum up to index i
    public int query(int i) {
        i++; // BITree is 1-indexed
        int sum = 0;
        while (i > 0) {
            sum += BITree[i];
            i -= i & -i;
        }
        return sum;
    }

    // Get sum of elements in range [start, end]
    public int rangeQuery(int start, int end) {
        return query(end) - query(start - 1);
    }

    // Print the Binary Indexed Tree with structure
    public void printTree() {
        System.out.println("Binary Indexed Tree:");
        for (int i = 1; i <= size; i++) {
            System.out.println("Index " + i + ": " + BITree[i] + " (Parent: " + getParent(i) + ")");
        }
        System.out.println();
    }

    // Get parent index of a node
    private int getParent(int index) {
        return index - (index & -index);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int n = arr.length;

        BinaryIndexedTree BITree = new BinaryIndexedTree(n);

        // Constructing the BITree
        for (int i = 0; i < n; i++) {
            BITree.update(i, arr[i]);
        }

        // Print the initial BITree
        BITree.printTree();

        // Querying prefix sums
        System.out.println("Prefix sum up to index 5: " + BITree.query(5)); // Output: 1+2+3+4+5 = 15
        
        
        // Updating an element
        int indexToUpdate = 3;
        int delta = 3; // Adding 3 to the value at index 3
        arr[indexToUpdate] += delta;
        BITree.update(indexToUpdate, delta);

        // Print the updated BITree
        BITree.printTree();

        // Querying prefix sums after update
        System.out.println("Prefix sum up to index 5 after update: " + BITree.query()); // Output: 1+2+6+4+5 = 18
    }
}

```
```
class BinaryIndexedTree2D {
    private int[][] BITree;
    private int rows;
    private int cols;

    public BinaryIndexedTree2D(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.BITree = new int[rows + 1][cols + 1];
    }

    // Update element at position (x, y) by adding val
    public void update(int x, int y, int val) {
        for (int i = x; i <= rows; i += i & -i) {
            for (int j = y; j <= cols; j += j & -j) {
                BITree[i][j] += val;
            }
        }
    }

    // Get sum of elements in rectangle with top-left corner (x1, y1) and bottom-right corner (x2, y2)
    public int query(int x1, int y1, int x2, int y2) {
        return query(x2, y2) - query(x1 - 1, y2) - query(x2, y1 - 1) + query(x1 - 1, y1 - 1);
    }

    // Get prefix sum up to position (x, y)
    private int query(int x, int y) {
        int sum = 0;
        for (int i = x; i > 0; i -= i & -i) {
            for (int j = y; j > 0; j -= j & -j) {
                sum += BITree[i][j];
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        int[][] arr = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12}
        };
        int rows = arr.length;
        int cols = arr[0].length;

        BinaryIndexedTree2D BITree = new BinaryIndexedTree2D(rows, cols);

        // Constructing the 2D BITree
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                BITree.update(i + 1, j + 1, arr[i][j]);
            }
        }

        // Querying sum in rectangle (1, 1) to (2, 2)
        System.out.println("Sum in rectangle (1, 1) to (2, 2): " + BITree.query(1, 1, 2, 2)); // Output: 21

        // Updating an element at position (2, 2)
        int x = 2;
        int y = 2;
        int delta = 3; // Adding 3 to the value at position (2, 2)
        arr[x][y] += delta;
        BITree.update(x + 1, y + 1, delta);

        // Querying sum in rectangle (1, 1) to (2, 2) after update
        System.out.println("Sum in rectangle (1, 1) to (2, 2) after update: " + BITree.query(1, 1, 2, 2)); // Output: 24
    }
}

```