```
public static <T> void testX(T result, T expected, String msg) {
    if (result.equals(expected)) {
        System.out.println("Test passed: " + msg + ", result = " + result);
    } else {
        System.out.println("Test failed: " + msg + ", result = " + result + ", expected = " + expected);
    }
}
```
```
    public static int search(int[] nums, int t) {
        int l = -1, r = nums.length;
        while (l + 1 != r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] >= t) {
                r = mid;
            } else {
                l = mid;
            }
        }
        return r;
    }
```

```
    public static void print(Object... args) {
        for (Object arg : args) {
            System.out.print(arg);
            System.out.print(",");
        }
        System.out.println();
    }
    
```

```
    linkedList = linkedList.stream()
                              .filter(element -> element % 2 == 0)
                              .collect(Collectors.toCollection(LinkedList::new));

```

```
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Example {
    private int count = 0;
    private Lock lock = new ReentrantLock();

    // Method using ReentrantLock
    public void increment() {
        lock.lock();
        try {
            count++;
        } finally {
            lock.unlock();
        }
    }
}

```
```
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
    ListNode reverse(ListNode head, int cnt) {
        ListNode prev = null;
        ListNode current = head;
        ListNode nextNode;

        while (cnt > 0) {
            nextNode = current.next;
            current.next = prev;
            prev = current;
            current = nextNode;
            cnt--;
        }
        head.next=current;

        return prev;
    }
    public ListNode reverseKGroup(ListNode head, int k) {
        var p1 = head;
        var p2 = head;
        for (int i=0;i<k;i++) {
            if (p2 == null) return head;
            p2 = p2.next;
        }
        var newHead = reverse(p1, k);
        head.next = reverseKGroup(p2, k);
        return newHead;
    }
    
```
